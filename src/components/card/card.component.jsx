import React from 'react';

import './card.styles.css'

export const Card = props => (
    <div className='card-container'>
        <img alt="product" src={`https://robohash.org/${props.product.id}?set=set2&size=180x180`}/>
        <h1>{props.product.name}</h1>
    </div>
)